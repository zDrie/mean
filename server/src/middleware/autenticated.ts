import jwt from 'jwt-simple';
import moment from 'moment';
import { Request, Response} from 'express'
var secret = 'clave_secreta_curso'

function ensureAuth(req:Request, res:Response, next:Function){
    if(!req.headers.authorization){
         res.status(401).send('la petición no tiene la cabecera de autenticación');
    }else{
        var token = req.headers.authorization.replace(/['"]+/g, '');
        try{
            var payload = jwt.decode(token, secret);
            if(payload.exp <= moment().unix()){
                res.status(401).send('el token ha expirado');                
            }else{
                req.user = payload;
                next();
            }
        }catch(ex){
            console.log(ex);
            res.status(404).send('el token no es válido');

        }

    }
}

export default ensureAuth;