import {Router} from 'express';
import indexController from '../controllers/indexController';


class IndexRoutes {

    public router:Router = Router();

    constructor(){
        this.config();
    }
    config():void{
        //obtener
        this.router.get('/', indexController.index);
        //crear
        this.router.post('/', indexController.index);
        //modificar
        this.router.put('/:id', indexController.index);
        //eliminar
        this.router.delete('/:id', indexController.index);


    }
}

const indexRoutes = new IndexRoutes();
export default indexRoutes.router;