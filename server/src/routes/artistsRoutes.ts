import artistsController from '../controllers/artistsController';
import { Router } from 'express';
import md_auth from '../middleware/autenticated';
import upload from '../middleware/artistImages'

class ArtistsRoutes{
    public router: Router = Router();

    constructor() {
        this.config();
    }
    config(): void {

        //get
        this.router.get('/all/:page?', artistsController.getAll);
        this.router.get('/:id', artistsController.getOne);
        this.router.get('/get-image/:imageFile', artistsController.getImage);

        //post
        this.router.post('/upload-image/:id', md_auth, upload, artistsController.uploadImage);
        this.router.post('/', md_auth, artistsController.saveArtist);

        //put
        this.router.put('/update/:id', md_auth, artistsController.updateArtist);


        //delete
        this.router.delete('/:id', md_auth, artistsController.deleteArtist);
    }

}

const artistsRouter = new ArtistsRoutes();
export default artistsRouter.router;
