import { Request, Response } from 'express'
import Song from '../models/songModel';
import bcrypt from 'bcrypt';
import createToken from '../services/jwtService'
import fs from 'fs';
import path from 'path';
import { inflate } from 'zlib';

class SongsController {

    getAll(req: Request, res: Response) {
        var albumId = req.params.album;

        if (!albumId) {
            Song
                .find({})
                .populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } })
                .sort('number')
                .exec((err, songs) => {
                    if (err) {
                        res.status(500).send('error en la petición');
                    } else {
                        if (songs) {
                            res.status(200).send(songs);
                        } else {
                            res.status(404).send('No hay canciones');
                        }
                    }
                })
        } else {
            Song
                .find({ album: albumId })
                .populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } })
                .sort('number')
                .exec((err, songs) => {
                    if (err) {
                        res.status(500).send('error en la petición');
                    } else {
                        if (songs) {
                            res.status(200).send(songs);
                        } else {
                            res.status(404).send('No hay canciones');
                        }
                    }
                })
        }
    }

    getOne(req: Request, res: Response) {
        var songId = req.params.id;
        Song.findById(songId).populate({ path: 'album' }).exec((err, song) => {
            if (err) {
                res.status(500).send('Error en la petición');
            } else {
                if (song) {
                    res.status(200).send(song);
                } else {
                    res.status(404).send('la canción no existe');
                }

            }
        });
    }

    saveSong(req: Request, res: Response) {
        var song = new Song();
        var params = req.body;
        song.number = params.number;
        song.name = params.name;
        song.duration = params.duration;
        song.file = 'null';
        song.album = params.album;

        song.save((err: Error, songStored: any) => {
            if (err) {
                res.status(500).send('error al guardar la canción');
            } else {
                if (songStored) {
                    res.status(200).send(songStored);
                } else {
                    res.status(404).send('la canción no ha sido guardada');
                }
            }
        })
    }

    updateSong(req: Request, res: Response) {
        var songID = req.params.id;
        var number = req.body.number
        var duration = req.body.duration
        var name = req.body.name;
        var album = req.body.album;
        var file = req.body.file;
        Song.findByIdAndUpdate(songID, { $set: { 'number': number, 'name': name, 'album': album, 'duration': duration, 'file': file } }, { new: true }, (err: Error, songUpdated) => {
            if (err) {
                res.status(500).send('error al actualizar la canción')
                throw err;
            } else {
                if (!songUpdated) {
                    res.status(404).send('no se ha podido actualizar la canción')
                } else {
                    res.status(200).send(songUpdated);
                }
            }
        });
    }

    deleteSong(req: Request, res: Response) {
        var songID = req.params.id;
        Song.findByIdAndRemove(songID, (err, songRemoved) => {
            if (err) {
                res.status(500).send('error al eliminar la canción');
            } else {
                if (songRemoved) {
                    res.status(200).send(songRemoved);
                } else {
                    res.status(404).send('no se ha eliminado la canción')
                }

            }
        });
    }

    uploadAudio(req: Request, res: Response) {

        if (req.file) {
            if (req.file)
                Song.findByIdAndUpdate(req.params.id, { 'file': req.file.filename }, { new: true }, (err, songUpdated) => {
                    if (songUpdated) {
                        res.status(200).send(songUpdated);
                    } else {
                        res.status(404).send('se ha subido el archivo pero no se ha podido actualizar la canción')
                    }
                });
        } else {
            res.status(200).send('no se ha subido ningún archivo');
        }
    }
    getAudio(req: Request, res: Response) {
        var songId = req.params.id;
        var dirname = '';
        Song.findById(songId).populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } }).exec((err, song) => {
            if (err){
                res.status(500).send('error al buscar el archivo');
            }else{
                dirname = `./src/public/uploads/artists/${song.album.artist.name}/${song.album.title}/${song.number}-${song.name}.mp3`;
                fs.exists(dirname, (exists) => {
                    if (exists) {
                        res.status(200).sendFile(path.resolve(dirname));
                    } else {
                        res.status(404).send('no existe el archivo');
                    }
                });
            }
        });
    }
}

const songsController = new SongsController();
export default songsController;