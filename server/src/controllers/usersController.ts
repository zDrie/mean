import { Request, Response } from 'express'
import User from '../models/userModel';
import bcrypt from 'bcrypt';
import createToken from '../services/jwtService'
import fs from 'fs';
import path from 'path';

class UsersController {

    index(req: Request, res: Response) {
        res.status(200).json({ text: 'probando el controlador de usuarios' })
    }

    saveUser(req: Request, res: Response) {
        const params = req.body;

        //si me enviaron una contraseña la encripto
        // esto se está encriptando cuando llega al servidor?
        // no deberia encriptarse en el cliente(?)

        if (params.name && params.surname && params.email && params.password) {
            var user = new User();
            user.set('name', params.name);
            user.set('surname', params.surname);
            user.set('email', params.email);
            user.set('rol', 'USER');
            user.set('image', 'music.png');
            bcrypt.hash(params.password, 3, (err: Error, hash: String) => {
                if (err) {
                    res.send(err);
                    throw err;
                } else {
                    user.set('password', hash);
                    user.save((err, userStored) => {
                        if (err) {
                            res.status(500).send('error al guardar el usuario')
                        } else {
                            if (userStored) {
                                res.status(200).send(userStored.toJSON());
                            } else {
                                res.status(404).send('no se ha registrado el usuario')
                            }
                        }
                    });
                }
            })

        } else {
            res.send('introduce todos los campos');
        }
    }

    loginUser(req: Request, res: Response) {
        const params = req.body;
        var email = params.email;
        var password = params.password;


        User.findOne({ email: email }, (err: Error, user) => {
            if (err) {
                res.status(500).send('Error en la peticion')
            } else {
                if (user) {
                    bcrypt.compare(password, user.password, (err, check) => {
                        if (check) {
                            if (params.getHash) {
                                //devolver un jwt
                                res.status(200).send({
                                    token: createToken(user)
                                })
                            } else {
                                res.status(200).send({ user });
                            }
                        } else {
                            res.status(500).send('contraseña o email incorrectos')
                        }
                    })
                } else {
                    res.status(500).send('el usuario no existe');
                }
            }
        })
    }

    updateUser(req: Request, res: Response) {
        //deja actualizar rol y otras webadas que no deberia
        // plox agregar
        var userID = req.params.id;
        var name = req.body.name;
        var surname = req.body.surname;
        var email = req.body.email;
        var image = req.body.image;
        User.findByIdAndUpdate(userID, { $set: { 'name': name, 'surname': surname, 'email': email, 'image': image } }, { new: true }, (err, userUpdated) => {
            if (err) {
                res.status(500).send('error al actualizar el usuario')
                throw err;
            } else {
                if (!userUpdated) {
                    res.status(404).send('no se ha podido actualizar el usuario')
                } else {
                    console.log('user send');
                    res.status(200).send(userUpdated);
                }
            }
        });
    }

    uploadImage(req: Request, res: Response) {
        var userId = req.params.id;

        if (req.file) {
            if (req.file)
                User.findByIdAndUpdate(req.params.id, { 'image': req.file.filename }, { new: true }, (err, userUpdated) => {
                    if (userUpdated) {
                        res.status(200).send(userUpdated);
                    } else {
                        res.status(404).send('se ha subido la imagen pero no se ha podido actualizar el usuario')
                    }
                });
        } else {
            res.status(200).send('no se ha subido ninguna imagen');
        }
    }
    getAvatar(req: Request, res: Response) {
        var imagefile = req.params.imageFile;
        const file = './src/public/uploads/users/images/' + imagefile;
        console.log(file);

        fs.exists(file, (exists) => {
            if (exists) {
                res.status(200).sendFile(path.resolve(file))
            } else {
                res.status(404).send('no existe la imagen')
            }
        })
    }

}
const usersController = new UsersController();
export default usersController;