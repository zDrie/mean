import path from 'path';
import fs from 'fs';
import { Request, Response } from 'express';
import Album from '../models/albumModel';
import Song from '../models/songModel';

class AlbumsController {

    getAll(req: Request, res: Response) {

        var artistId = req.params.artist;
        var page = req.params.page || 1;
        var itemsPerPage = 8;

        if (!artistId) {
            Album
                .find({})
                .populate({ path: 'artist' })
                .sort('title')
                .skip((itemsPerPage * (page as number)) - itemsPerPage)
                .limit(itemsPerPage)
                .exec((err, albums) => {
                    if (err) {
                        res.status(500).send('hubo un error en la petición')
                    } else {
                        if (albums) {
                            Album.estimatedDocumentCount((err: Error, count) => {
                                if (err) {
                                    res.status(404).send('no hay albums');
                                } else {
                                    var pages = Math.ceil(count / itemsPerPage)
                                    res.status(200).send({
                                        pages: pages,
                                        albums: albums
                                    });
                                }
                            });
                        } else {
                            res.status(404).send('no se han podido obtener los albums')
                        }
                    }
                });
        } else {
            Album
                .find({ artist: artistId })
                .populate({ path: 'artist' })
                .sort('year')
                .skip((itemsPerPage * (page as number)) - itemsPerPage)
                .limit(itemsPerPage)
                .exec((err, albums) => {
                    if (err) {
                        res.status(500).send('hubo un error en la petición')
                    } else {
                        if (albums) {
                            Album.estimatedDocumentCount((err: Error, count) => {
                                if (err) {
                                    res.status(404).send('no hay albums');
                                } else {
                                    var pages = Math.ceil(count / itemsPerPage)
                                    res.status(200).send({
                                        pages: pages,
                                        albums: albums
                                    });
                                }
                            });
                        } else {
                            res.status(404).send('no se han podido obtener los albums');
                        }
                    }
                });
        }
    }

    getOne(req: Request, res: Response) {
        var albumId = req.params.id;
        Album.findById(albumId).populate({ path: 'artist' }).exec((err, album) => {
            if (err) {
                res.status(500).send('Error en la petición')
            } else {
                if (album) {
                    res.status(200).send(album);
                } else {
                    res.status(404).send('el album no existe')
                }

            }
        });
    }

    saveAlbum(req: Request, res: Response) {
        var album = new Album();
        var params = req.body;
        album.title = params.title;
        album.description = params.description;
        album.year = params.year;
        album.image = 'music.png';
        album.artist = params.artist;

        album.save((err: Error, albumStored: any) => {
            if (err) {
                res.status(500).send('error al guardar el album');
            } else {
                if (albumStored) {
                    res.status(200).send(albumStored);
                } else {
                    res.status(404).send('el album no ha sido guardado');
                }
            }
        })
    }

    updateAlbum(req: Request, res: Response) {
        var albumID = req.params.id;
        var title = req.body.title;
        var description = req.body.description;
        var year = req.body.year;
        var artist = req.body.artist;
        var image = req.body.image;
        Album.findByIdAndUpdate(albumID, { $set: { 'title': title, 'year': year, 'artist': artist, 'description': description, 'image': image } }, { new: true }, (err: Error, albumUpdated) => {
            if (err) {
                res.status(500).send('error al actualizar el album')
                throw err;
            } else {
                if (!albumUpdated) {
                    res.status(404).send('no se ha podido actualizar el album')
                } else {
                    res.status(200).send(albumUpdated);
                }
            }
        });
    }

    // deleteAlbum(req: Request, res: Response) {
    //     var albumID = req.params.id;
    //     Album.findByIdAndRemove(albumID, (err, albumRemoved) => {
    //         if (err) {
    //             res.status(500).send('error al eliminar el album');
    //         } else {
    //             if (albumRemoved) {
    //                 Album.find({ 'artist': albumRemoved._id }).remove((err: Error, songRemoved) => {
    //                     if (err) {
    //                         res.status(500).send('error al eliminar la canción')
    //                     } else {
    //                         if (songRemoved) {
    //                             res.status(200).send(songRemoved);
    //                         } else {
    //                             res.status(404).send('la canción no ha sido eliminada');
    //                         }
    //                     }
    //                 });
    //             } else {
    //                 res.status(404).send('el album no ha sido eliminado')
    //             }
    //         }
    //         res.status(200).send(albumRemoved);
    //     });
    // }

    deleteAlbum(req: Request, res: Response) {
        var artID = req.params.id;
        Album.findByIdAndRemove(artID, (err, artRemoved) => {
        if (err){
            res.status(500).send('error al eliminar el album');
        } else{
            if (artRemoved){
                Song.find({'album': artRemoved._id}, (err, res) => {
                    res.forEach((item)=>{
                        Song.deleteMany({'album': item._id});
                    })
                    
                });
                res.status(200).send(artRemoved);
            } else {

            }
        }
        });
            
    }

    uploadImage(req: Request, res: Response) {

        if (req.file) {
            if (req.file)
                Album.findByIdAndUpdate(req.params.id, { 'image': req.file.filename }, { new: true }, (err, albumUpdated) => {
                    if (albumUpdated) {
                        res.status(200).send(albumUpdated);
                    } else {
                        res.status(404).send('se ha subido la imagen pero no se ha podido actualizar el usuario')
                    }
                });
        } else {
            res.status(200).send('no se ha subido ninguna imagen');
        }
    }

    getImage(req: Request, res: Response) {
        var imagefile = req.params.imageFile;
        const file = './src/public/uploads/albums/images/' + imagefile;

        fs.exists(file, (exists) => {
            if (exists) {
                res.status(200).sendFile(path.resolve(file))
            } else {
                res.status(404).send('no existe la imagen')
            }
        });
    }

}


const albumsController = new AlbumsController();
export default albumsController;