"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const artistModel_1 = __importDefault(require("../models/artistModel"));
class AlbumsController {
    getOne(req, res) {
        var albumId = req.params.id;
        artistModel_1.default.findById(albumId, (err, album) => {
            if (err) {
                res.status(500).send('Error en la petición');
            }
            else {
                if (album) {
                    res.status(200).send(album);
                }
                else {
                    res.status(404).send('el album no existe');
                }
            }
        });
    }
}
const albumsController = new AlbumsController();
exports.default = albumsController;
