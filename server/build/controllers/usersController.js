"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const userModel_1 = __importDefault(require("../models/userModel"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const jwtService_1 = __importDefault(require("../services/jwtService"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
class UsersController {
    index(req, res) {
        res.status(200).json({ text: 'probando el controlador de usuarios' });
    }
    saveUser(req, res) {
        const params = req.body;
        //si me enviaron una contraseña la encripto
        // esto se está encriptando cuando llega al servidor?
        // no deberia encriptarse en el cliente(?)
        if (params.name && params.surname && params.email && params.password) {
            var user = new userModel_1.default();
            user.set('name', params.name);
            user.set('surname', params.surname);
            user.set('email', params.email);
            user.set('rol', 'USER');
            user.set('image', 'music.png');
            bcrypt_1.default.hash(params.password, 3, (err, hash) => {
                if (err) {
                    res.send(err);
                    throw err;
                }
                else {
                    user.set('password', hash);
                    user.save((err, userStored) => {
                        if (err) {
                            res.status(500).send('error al guardar el usuario');
                        }
                        else {
                            if (userStored) {
                                res.status(200).send(userStored.toJSON());
                            }
                            else {
                                res.status(404).send('no se ha registrado el usuario');
                            }
                        }
                    });
                }
            });
        }
        else {
            res.send('introduce todos los campos');
        }
    }
    loginUser(req, res) {
        const params = req.body;
        var email = params.email;
        var password = params.password;
        userModel_1.default.findOne({ email: email }, (err, user) => {
            if (err) {
                res.status(500).send('Error en la peticion');
            }
            else {
                if (user) {
                    bcrypt_1.default.compare(password, user.password, (err, check) => {
                        if (check) {
                            if (params.getHash) {
                                //devolver un jwt
                                res.status(200).send({
                                    token: jwtService_1.default(user)
                                });
                            }
                            else {
                                res.status(200).send({ user });
                            }
                        }
                        else {
                            res.status(500).send('contraseña o email incorrectos');
                        }
                    });
                }
                else {
                    res.status(500).send('el usuario no existe');
                }
            }
        });
    }
    updateUser(req, res) {
        //deja actualizar rol y otras webadas que no deberia
        // plox agregar
        var userID = req.params.id;
        var name = req.body.name;
        var surname = req.body.surname;
        var email = req.body.email;
        var image = req.body.image;
        userModel_1.default.findByIdAndUpdate(userID, { $set: { 'name': name, 'surname': surname, 'email': email, 'image': image } }, { new: true }, (err, userUpdated) => {
            if (err) {
                res.status(500).send('error al actualizar el usuario');
                throw err;
            }
            else {
                if (!userUpdated) {
                    res.status(404).send('no se ha podido actualizar el usuario');
                }
                else {
                    console.log('user send');
                    res.status(200).send(userUpdated);
                }
            }
        });
    }
    uploadImage(req, res) {
        var userId = req.params.id;
        if (req.file) {
            if (req.file)
                userModel_1.default.findByIdAndUpdate(req.params.id, { 'image': req.file.filename }, { new: true }, (err, userUpdated) => {
                    if (userUpdated) {
                        res.status(200).send(userUpdated);
                    }
                    else {
                        res.status(404).send('se ha subido la imagen pero no se ha podido actualizar el usuario');
                    }
                });
        }
        else {
            res.status(200).send('no se ha subido ninguna imagen');
        }
    }
    getAvatar(req, res) {
        var imagefile = req.params.imageFile;
        const file = './src/public/uploads/users/images/' + imagefile;
        console.log(file);
        fs_1.default.exists(file, (exists) => {
            if (exists) {
                res.status(200).sendFile(path_1.default.resolve(file));
            }
            else {
                res.status(404).send('no existe la imagen');
            }
        });
    }
}
const usersController = new UsersController();
exports.default = usersController;
