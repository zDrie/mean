"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const artistModel_1 = __importDefault(require("../models/artistModel"));
const albumModel_1 = __importDefault(require("../models/albumModel"));
const songModel_1 = __importDefault(require("../models/songModel"));
class ArtistsController {
    getAll(req, res) {
        var page = req.params.page || 1;
        var itemsPerPage = 6;
        artistModel_1.default
            .find({})
            .sort('name')
            .skip((itemsPerPage * page) - itemsPerPage)
            .limit(itemsPerPage)
            .exec((err, artists) => {
            if (err) {
                res.status(500).send('hubo un error en la petición');
            }
            else {
                if (artists) {
                    artistModel_1.default.estimatedDocumentCount((err, count) => {
                        if (err) {
                            res.status(404).send('no hay artistas');
                        }
                        else {
                            var pages = Math.ceil(count / itemsPerPage);
                            res.status(200).send({
                                pages: pages,
                                artists: artists
                            });
                        }
                    });
                }
                else {
                    res.status(404).send('no se han podido obtener los artistas');
                }
            }
        });
    }
    getOne(req, res) {
        var artistId = req.params.id;
        artistModel_1.default.findById(artistId, (err, artist) => {
            if (err) {
                res.status(500).send('Error en la petición');
            }
            else {
                if (artist) {
                    res.status(200).send(artist);
                }
                else {
                    res.status(404).send('el artista no existe');
                }
            }
        });
    }
    saveArtist(req, res) {
        var artist = new artistModel_1.default();
        var params = req.body;
        artist.name = params.name;
        artist.description = params.description;
        artist.image = 'music.png';
        artist.save((err, artistStored) => {
            if (err) {
                res.status(500).send('error al guardar el artista');
            }
            else {
                if (artistStored) {
                    res.status(200).send(artistStored);
                }
                else {
                    res.status(404).send('el artista no ha sido guardado');
                }
            }
        });
    }
    updateArtist(req, res) {
        var artID = req.params.id;
        var name = req.body.name;
        var description = req.body.description;
        var image = req.body.image;
        artistModel_1.default.findByIdAndUpdate(artID, { $set: { 'name': name, 'description': description, 'image': image } }, { new: true }, (err, artistUpdated) => {
            if (err) {
                res.status(500).send('error al actualizar el artista');
                throw err;
            }
            else {
                if (!artistUpdated) {
                    res.status(404).send('no se ha podido actualizar el artista');
                }
                else {
                    res.status(200).send(artistUpdated);
                }
            }
        });
    }
    // deleteArtist(req: Request, res: Response) {
    //     var artID = req.params.id;
    //     Artist.findByIdAndRemove(artID, (err, artRemoved) => {
    //         if (err) {
    //             res.status(500).send('error al eliminar el artista');
    //         } else {
    //             if (artRemoved) {
    //                 Album.deleteOne({ 'artist': artRemoved._id }, (err, obj) => {
    //                     if (err) {
    //                         res.status(500).send('error al eliminar el album');
    //                     } else {
    //                         if (albRemoved) {
    //                             res.status(200).send(albRemoved);
    //                             Song.find({ 'album': albRemoved._id }).remove((err: Error, songRemoved: any) => {
    //                                 if (err) {
    //                                     res.status(500).send('error al eliminar la canción')
    //                                 } else {
    //                                     if (songRemoved) {
    //                                         res.status(200).send(songRemoved);
    //                                     } else {
    //                                         res.status(404).send('la canción no ha sido eliminada');
    //                                     }
    //                                 }
    //                             })
    //                         } else {
    //                             res.status(404).send('el album no ha sido eliminad')
    //                         }
    //                     }
    //                 });
    //                 res.status(200).send(artRemoved);
    //             } else {
    //                 res.status(404).send('el artista no se ha eliminado');
    //             }
    //         }
    //     });
    // }
    deleteArtist(req, res) {
        var artID = req.params.id;
        artistModel_1.default.findByIdAndRemove(artID, (err, artRemoved) => {
            if (err) {
                res.status(500).send('error al eliminar el album');
            }
            else {
                if (artRemoved) {
                    albumModel_1.default.find({ 'artist': artRemoved._id }, (err, res) => {
                        res.forEach((item) => {
                            songModel_1.default.deleteMany({ 'album': item._id });
                        });
                    });
                    albumModel_1.default.deleteMany({ 'artist': artRemoved._id });
                    res.status(200).send(artRemoved);
                }
                else {
                }
            }
        });
    }
    uploadImage(req, res) {
        if (req.file) {
            if (req.file)
                artistModel_1.default.findByIdAndUpdate(req.params.id, { 'image': req.file.filename }, { new: true }, (err, artistUpdated) => {
                    if (artistUpdated) {
                        res.status(200).send(artistUpdated);
                    }
                    else {
                        res.status(404).send('se ha subido la imagen pero no se ha podido actualizar el usuario');
                    }
                });
        }
        else {
            res.status(200).send('no se ha subido ninguna imagen');
        }
    }
    getImage(req, res) {
        var imagefile = req.params.imageFile;
        const file = './src/public/uploads/artists/images/' + imagefile;
        console.log(file);
        fs_1.default.exists(file, (exists) => {
            if (exists) {
                res.status(200).sendFile(path_1.default.resolve(file));
            }
            else {
                res.status(404).send('no existe la imagen');
            }
        });
    }
}
const artistsController = new ArtistsController();
exports.default = artistsController;
