"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const colors_1 = __importDefault(require("colors"));
const morgan_1 = __importDefault(require("morgan"));
const mongoose_1 = __importDefault(require("mongoose"));
const indexRoutes_1 = __importDefault(require("./routes/indexRoutes"));
const usersRoutes_1 = __importDefault(require("./routes/usersRoutes"));
const artistsRoutes_1 = __importDefault(require("./routes/artistsRoutes"));
const albumsRoutes_1 = __importDefault(require("./routes/albumsRoutes"));
const songsRoutes_1 = __importDefault(require("./routes/songsRoutes"));
const body_parser_1 = require("body-parser");
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    config() {
        this.app.set('port', process.env.PORT || 3000);
        this.app.use(morgan_1.default('dev'));
        this.app.use(express_1.default.json());
        this.app.use(body_parser_1.urlencoded({ extended: false }));
        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            req.header('Access-Control-Allow-Origin');
            res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
            res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
            res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
            next();
        });
    }
    routes() {
        this.app.use('/', indexRoutes_1.default);
        this.app.use('/api/users', usersRoutes_1.default);
        this.app.use('/api/artists', artistsRoutes_1.default);
        this.app.use('/api/albums', albumsRoutes_1.default);
        this.app.use('/api/songs', songsRoutes_1.default);
    }
    //conectando a la base de datos
    start() {
        const connectDb = () => {
            return mongoose_1.default.connect('mongodb://localhost:27017/cursomean', {
                useNewUrlParser: true,
                useFindAndModify: false,
                useUnifiedTopology: true
            }, (err) => {
                if (err) {
                    throw err;
                }
                else {
                    console.log('la base de datos está conectada'.america.inverse.italic);
                }
            });
        };
        connectDb().then(() => {
            this.app.listen(this.app.get('port'), () => {
                console.log(colors_1.default.random(`Server ON Port ${this.app.get('port')}`));
            });
        });
    }
}
const server = new Server();
server.start();
