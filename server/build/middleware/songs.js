"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const multer_1 = __importDefault(require("multer"));
const songModel_1 = __importDefault(require("../models/songModel"));
const fs_1 = __importDefault(require("fs"));
var dirname = '';
var newName = '';
var upload = multer_1.default({
    storage: multer_1.default.diskStorage({
        destination: (req, file, callBack) => {
            var songId = req.params.id;
            songModel_1.default.findById(songId).populate({ path: 'album', populate: { path: 'artist', model: 'Artist' } }).exec((err, song) => {
                dirname = `./src/public/uploads/artists/${song.album.artist.name}/${song.album.title}/`;
                if (!fs_1.default.existsSync(dirname)) {
                    fs_1.default.mkdirSync(dirname, { recursive: true });
                }
                callBack(null, dirname);
            });
        },
        filename: (req, file, callBack) => {
            var songId = req.params.id;
            songModel_1.default.findById(songId).exec((err, song) => {
                if (err) {
                    return callBack(err, 'false');
                }
                var ext = file.originalname.split('\.');
                var fileExt = ext[1];
                newName = song.number + '-' + song.name + '.' + fileExt;
                callBack(null, `${newName}`);
            });
        },
    }),
    fileFilter: (req, file, callBack) => {
        var ext = file.originalname.split('\.');
        var fileExt = ext[1];
        if (fileExt == 'mp3' || fileExt == 'wma' || fileExt == 'wav' || fileExt == 'mp4') {
            return callBack(null, true);
        }
        else {
            req.fileValidationError = 'el archivo no es del tipo esperado';
            return callBack(new Error('el archivo no es del tipo esperado'), false);
        }
    }
}).single('file');
exports.default = upload;
