"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const multer_1 = __importDefault(require("multer"));
var storage1 = multer_1.default.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './src/public/uploads/albums/images');
    },
    filename: (req, file, callBack) => {
        var ext = file.originalname.split('\.');
        var fileExt = ext[1];
        var newName = req.params.id + '.' + fileExt;
        callBack(null, `portada_${newName}`);
    },
});
var upload = multer_1.default({
    storage: storage1,
    fileFilter: (req, file, callBack) => {
        var ext = file.originalname.split('\.');
        var fileExt = ext[1];
        if (fileExt == 'png' || fileExt == 'jpg' || fileExt == 'gif') {
            return callBack(null, true);
        }
        else {
            req.fileValidationError = 'el archivo no es del tipo esperado';
            return callBack(new Error('el archivo no es del tipo esperado'), false);
        }
    }
}).single('image');
exports.default = upload;
