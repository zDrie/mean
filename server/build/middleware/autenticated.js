"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_simple_1 = __importDefault(require("jwt-simple"));
const moment_1 = __importDefault(require("moment"));
var secret = 'clave_secreta_curso';
function ensureAuth(req, res, next) {
    if (!req.headers.authorization) {
        res.status(401).send('la petición no tiene la cabecera de autenticación');
    }
    else {
        var token = req.headers.authorization.replace(/['"]+/g, '');
        try {
            var payload = jwt_simple_1.default.decode(token, secret);
            if (payload.exp <= moment_1.default().unix()) {
                res.status(401).send('el token ha expirado');
            }
            else {
                req.user = payload;
                next();
            }
        }
        catch (ex) {
            console.log(ex);
            res.status(404).send('el token no es válido');
        }
    }
}
exports.default = ensureAuth;
