"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Schema = mongoose_1.default.Schema;
const ArtistSchema = new Schema({
    name: String,
    description: String,
    image: String
});
const Artist = mongoose_1.default.model('Artist', ArtistSchema, 'Artists');
exports.default = Artist;
