"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const mongodb_1 = require("mongodb");
const Schema = mongoose_1.default.Schema;
const SongSchema = new Schema({
    number: String,
    name: String,
    duration: String,
    file: String,
    album: { type: mongodb_1.ObjectId, ref: 'Album' }
});
exports.default = mongoose_1.default.model('Song', SongSchema, 'Songs');
