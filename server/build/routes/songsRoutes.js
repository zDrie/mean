"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const songsController_1 = __importDefault(require("../controllers/songsController"));
const express_1 = require("express");
const autenticated_1 = __importDefault(require("../middleware/autenticated"));
const songs_1 = __importDefault(require("../middleware/songs"));
class ArtistsRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        //get
        this.router.get('/all/:album?', songsController_1.default.getAll);
        this.router.get('/:id', songsController_1.default.getOne);
        this.router.get('/get-file/:id', songsController_1.default.getAudio);
        //post
        this.router.post('/', songsController_1.default.saveSong);
        this.router.post('/upload-file/:id', songs_1.default, songsController_1.default.uploadAudio);
        //put
        this.router.put('/update/:id', autenticated_1.default, songsController_1.default.updateSong);
        //delete
        this.router.delete('/:id', autenticated_1.default, songsController_1.default.deleteSong);
    }
}
const albumsRouter = new ArtistsRoutes();
exports.default = albumsRouter.router;
