"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const indexController_1 = __importDefault(require("../controllers/indexController"));
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        //obtener
        this.router.get('/', indexController_1.default.index);
        //crear
        this.router.post('/', indexController_1.default.index);
        //modificar
        this.router.put('/:id', indexController_1.default.index);
        //eliminar
        this.router.delete('/:id', indexController_1.default.index);
    }
}
const indexRoutes = new IndexRoutes();
exports.default = indexRoutes.router;
