"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const usersController_1 = __importDefault(require("../controllers/usersController"));
const autenticated_1 = __importDefault(require("../middleware/autenticated"));
const images_1 = __importDefault(require("../middleware/images"));
class UsersRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        //obtener
        this.router.get('/probando', autenticated_1.default, usersController_1.default.index);
        this.router.get('/get-avatar/:imageFile', usersController_1.default.getAvatar);
        //crear
        this.router.post('/register', usersController_1.default.saveUser);
        this.router.post('/login', usersController_1.default.loginUser);
        this.router.post('/upload-image/:id', autenticated_1.default, function (req, res, next) {
            images_1.default(req, res, function (err) {
                if (req.fileValidationError) {
                    return res.status(500).end('el archivo no es del tipo esperado');
                }
                else {
                    next();
                }
            });
        }, usersController_1.default.uploadImage);
        //modificar
        this.router.put('/update/:id', autenticated_1.default, usersController_1.default.updateUser);
        //eliminar
        this.router.delete('/:id', autenticated_1.default, usersController_1.default.index);
    }
}
const usersRoutes = new UsersRoutes();
exports.default = usersRoutes.router;
