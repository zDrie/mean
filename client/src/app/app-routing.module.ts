import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'principal/startmenu/artists/', pathMatch: 'full' },
  { path: 'login', loadChildren: '../app/components/login/login.module#LoginPageModule'},
  { path: 'principal', loadChildren: '../app/components/principal/principal.module#PrincipalPageModule' },
  { path: '**', redirectTo: 'principal/startmenu/artists/'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
