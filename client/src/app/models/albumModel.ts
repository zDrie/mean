
export default class AlbumModel {
    constructor(
    public _id: string,
    public title: string,
    public year: number,
    public description: string,
    public image: string,
    public artist: string
    ) {}
}


