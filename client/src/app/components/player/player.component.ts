import { Component, OnInit } from '@angular/core';
import { SongService } from 'src/app/services/song.service';
import SongModel from 'src/app/models/songModel';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit {
  song: SongModel = {
    _id: '',
    number: null,
    name: '',
    duration: '',
    file: '',
    album: ''
  };
  constructor(private songService: SongService) { }

  ngOnInit() {
    const source = document.getElementById('playersource');
    const image = document.getElementById('songimage');
    const player = document.getElementById('player');
    const pe = document.getElementById('pe');
    this.songService.song$.subscribe(song => {
      this.song = JSON.parse(song);
      image.setAttribute('src', 'http://localhost:3000/api/albums/get-image/' + (this.song.album as any).image );
      source.setAttribute('src', 'http://localhost:3000/api/songs/get-file/' + this.song._id );
      pe.innerHTML = (this.song.album as any).artist.name;
      (player as any).load();
      (player as any).play();
    });
  }

}
