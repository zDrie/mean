import { Component, OnInit, ElementRef } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import SongModel from 'src/app/models/songModel';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlbumService } from 'src/app/services/album.service';
import { AlertController } from '@ionic/angular';
import { ArtistService } from 'src/app/services/artist.service';
import { SongService } from 'src/app/services/song.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  identity;
  token;
  filesToUpload;
  guardar;





  // tslint:disable-next-line: max-line-length
  constructor(private userService: UserService, private songService: SongService, private activatedRoute: ActivatedRoute, private albumService: AlbumService, private alertCtrl: AlertController, private artistService: ArtistService, private router: Router, private formBuilder: FormBuilder) {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.token = this.userService.getToken();


    if (!this.identity || !this.token) {
      this.router.navigate(['/login']);
    }
    if (this.identity.rol !== 'ADMIN') {
      this.router.navigate(['/']);
    }

  }
  get name() {
    return this.addForm.get('name');
  }

  get duration() {
    return this.addForm.get('duration');
  }

  get number() {
    return this.addForm.get('number');
  }

  public errorMesssages = {
    name: [{ type: 'required', message: 'Name is required' },
    { type: 'minlength', message: 'Name cant be smaller than 1 characters' },
    { type: 'maxlength', message: 'Name cant be longer than 40 characters' }],
    number: [{ type: 'required', message: 'Number is required' },
    { type: 'pattern', message: 'Please enter a valid number NN' }],
    duration: [{ type: 'required', message: 'Duration is required' },
    { type: 'pattern', message: 'Please enter a valid duraton MM:SS' }
    ]
  };

  song: SongModel = {
    _id: '',
    number: null,
    name: '',
    duration: '',
    file: '',
    album: this.activatedRoute.snapshot.params.id
  };

  addForm = this.formBuilder.group({
    name: [this.song.name, [Validators.required, Validators.maxLength(40), Validators.minLength(1)]],
    number: [this.song.number, [Validators.required, Validators.pattern('^[0-9-]*$')]],
    duration: [this.song.duration, [Validators.required, Validators.pattern('^[0-9-]{2}:[0-9-]{2}$')]]
  });


  ngOnInit() {

  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }


  addSong() {
    this.song.name = this.addForm.get('name').value;
    this.song.duration = this.addForm.get('duration').value;
    this.song.number = this.addForm.get('number').value;


    this.songService.saveSong(this.song).subscribe((res: any) => {
      if (!res) {
        const err2 = new Error('La canción no se ha guardado');
        this.createAlert(err2);
      } else {
        this.song = res;

        if (this.filesToUpload) {
          this.songService.makeFileRequest([], this.filesToUpload, this.song._id).then((result) => {
            this.direccionar();
          });
        } else {
          this.direccionar();
        }
      }
    },
      (err) => {
        if (err) {
          this.createAlert(err);
        }
      });

  }

  salir() {
    this.router.navigate(['/']);
  }

  fileChamgeEvent(fileInput: any) {
    this.filesToUpload = fileInput.target.files as Array<File>;
  }

  direccionar() {
    this.song = {
      _id: '',
      number: null,
      name: '',
      duration: '',
      file: '',
      album: this.activatedRoute.snapshot.params.id
    };

    this.addForm.get('duration').reset();
    this.addForm.get('number').reset();
    this.addForm.get('name').reset();
    (document.getElementById('filesong') as any).value = '';
    if (this.guardar === 'otra') {
    } else {
      this.router.navigate(['/principal/starmenu/albums-detail/', this.activatedRoute.snapshot.params.id]);

    }
  }

}
