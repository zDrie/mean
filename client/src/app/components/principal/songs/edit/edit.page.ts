import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import SongModel from 'src/app/models/songModel';
import { SongService } from 'src/app/services/song.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  identity;
  token;
  filesToUpload;
  imageRef = 'http://localhost:3000/api/artists/get-image/';


  // tslint:disable-next-line: max-line-length
  constructor(private userService: UserService, private activatedRoute: ActivatedRoute, private alertCtrl: AlertController, private songService: SongService, private router: Router, private formBuilder: FormBuilder) {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.token = this.userService.getToken();
    this.getSong();

    if (!this.identity || !this.token) {
      this.router.navigate(['/login']);
    }
    if (this.identity.rol !== 'ADMIN') {
      this.router.navigate(['/']);
    }

  }

  get name() {
    return this.editForm.get('name');
  }

  get duration() {
    return this.editForm.get('duration');
  }

  get number() {
    return this.editForm.get('number');
  }

  public errorMesssages = {
    name: [{ type: 'required', message: 'Name is required' },
    { type: 'minlength', message: 'Name cant be smaller than 1 characters' },
    { type: 'maxlength', message: 'Name cant be longer than 40 characters' }],
    number: [{ type: 'required', message: 'Number is required' },
    { type: 'pattern', message: 'Please enter a valid number NN' }],
    duration: [{ type: 'required', message: 'Duration is required' },
    { type: 'pattern', message: 'Please enter a valid duraton MM:SS' }
    ]
  };

  song: SongModel = {
    _id: '',
    number: null,
    name: '',
    duration: '',
    file: '',
    album: ''
  };

  editForm = this.formBuilder.group({
    name: [this.song.name, [Validators.required, Validators.maxLength(40), Validators.minLength(1)]],
    number: [this.song.number, [Validators.required, Validators.pattern('^[0-9-]*$')]],
    duration: [this.song.duration, [Validators.required, Validators.pattern('^[0-9-]{2}:[0-9-]{2}$')]]
  });

  ngOnInit() {

  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }


  editSong() {
    this.song.name = this.editForm.get('name').value;
    this.song.duration = this.editForm.get('duration').value;
    this.song.number = this.editForm.get('number').value;

    this.songService.updateSong(this.song).subscribe((res: any) => {
      if (!res) {
        const err2 = new Error('El usuario no se ha actualizado');
        this.createAlert(err2);
      } else {
        this.song = res;

        if (this.filesToUpload) {
          this.songService.makeFileRequest([], this.filesToUpload, this.song._id)
            .then((result: any) => {
              this.router.navigate(['/principal/startmenu/albums-detail/', this.song.album]);

            });
        }
      }
    },
      (err: any) => {
        if (err) {
          this.createAlert(err);
        }
      }
    );

  }

  fileChangeEvent(fileInput: any) {
    this.filesToUpload = fileInput.target.files as Array<File>;
  }

  getSong() {
    const id = this.activatedRoute.snapshot.params.id;
    this.songService.getSong(id).subscribe((res: any) => {
      if (!res) {
        this.createAlert(new Error('No hay respuesta del servidor'));
        this.router.navigate(['/']);
      } else {
        this.song = res;
      }
    }, (err: any) => {
      if (err) {
        this.createAlert(err);
      }
    });
  }

  volver() {
    this.router.navigate(['/principal/startmenu/albums']);
  }
}
