import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-startmenu',
  templateUrl: './startmenu.page.html',
  styleUrls: ['./startmenu.page.scss'],
})
export class StartmenuPage implements OnInit {
  pages = [
    {
      title: 'ALBUMS',
      url: '/principal/startmenu/albums/1'
    },
    {
      title: 'ARTISTS',
      url: '/principal/startmenu/artists'
    },
    {
      title: 'YOU',
      url: '/principal/startmenu/you'
    }
  ];
  selectedPath = '/principal/startmenu/albums/1';
  identity;
  token;

  constructor(private router: Router, private userService: UserService) {
    this.router.events.subscribe( (event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
  }

  ngOnInit() {
    this.identity = this.userService.getIdentity();
    this.token = this.userService.getToken();

    if (this.identity == 'undefined' || this.token == 'undefined') {
      this.router.navigate(['/login']);
    }
  }

  logOut() {
    this.userService.logOut();
  }

}
