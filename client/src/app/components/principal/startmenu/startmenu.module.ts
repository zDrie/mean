import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { PlayerComponent } from '../../player/player.component';


import { IonicModule } from '@ionic/angular';

import { StartmenuPage } from './startmenu.page';

const routes: Routes = [
  {
    path: '',
    component: StartmenuPage,
    children: [ {
      path: 'songs-add/:id',
      loadChildren: '../songs/add/add.module#AddPageModule'
    },
    {
      path: 'songs-edit/:id',
      loadChildren: '../songs/edit/edit.module#EditPageModule'
    },
    {
      path: 'albums/:page',
      loadChildren: '../albums/albums.module#AlbumsPageModule'
    },
    {
      path: 'albums-add/:id',
      loadChildren: '../albums/add/add.module#AddPageModule'
    },
    {
      path: 'albums-edit/:id',
      loadChildren: '../albums/edit/edit.module#EditPageModule'
    },
    {
      path: 'albums-detail/:id',
      loadChildren: '../albums/detail/detail.module#DetailPageModule'
    },
    {
      path: 'artists/:page',
      loadChildren: '../artists/artists.module#ArtistsPageModule'
    },
    {
      path: 'you',
      loadChildren: '../user/user.module#UserPageModule'
    },
    {
      path: 'you/edit',
      loadChildren: '../user/edit/edit.module#EditPageModule'
    },
    {
      path: 'artists-add',
      loadChildren: '../artists/add/add.module#AddPageModule'
    },
    {
      path: 'artists-edit/:id',
      loadChildren: '../artists/edit/edit.module#EditPageModule'
    },
    {
      path: 'artists-detail/:id',
      loadChildren: '../artists/detail/detail.module#DetailPageModule'
    },

    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StartmenuPage, PlayerComponent],
  exports: [StartmenuPage],
  entryComponents: [StartmenuPage]
})
export class StartmenuPageModule { }
