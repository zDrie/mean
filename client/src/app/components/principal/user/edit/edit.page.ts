import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/app/models/userModel';
import { UserService } from 'src/app/services/user.service';
import { AlertController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {






  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder, private userService: UserService, private alertCtrl: AlertController, private router: Router) {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.user = this.identity;
  }

  get email() {
    return this.editForm.get('email');
  }
  get password() {
    return this.editForm.get('password');
  }
  get name() {
    return this.editForm.get('name');
  }
  get surname() {
    return this.editForm.get('surname');
  }
  get image() {
    return this.editForm.get('image');
  }

  identity;
  token;
  user = this.identity;
  editForm;
  imageRef = 'http://localhost:3000/api/users/get-avatar/';




  public errorMesssages = {
    email: [{ type: 'required', message: 'Email is required' },
    { type: 'pattern', message: 'Please enter a valid email adress' }],
    password: [{ type: 'required', message: 'Password is required' },
    { type: 'maxlength', message: 'Password cant be longer than 15 characters' }],
    name: [{ type: 'required', message: 'Name is required' },
    { type: 'minlength', message: 'Name cant be smaller than 2 characters' },
    { type: 'maxlength', message: 'Name cant be longer than 20 characters' }],
    surname: [{ type: 'required', message: 'Surname is required' },
    { type: 'minlength', message: 'Surname cant be smaller than 2 characters' },
    { type: 'maxlength', message: 'Surname cant be longer than 20 characters' }]
  };

  filesToUpload: Array<File>;


  ngOnInit() {
    this.user = this.identity;
    this.editForm = this.formBuilder.group({
      name: [this.user.name, [Validators.required, Validators.maxLength(20), Validators.minLength(2)]],
      surname: [this.user.surname, [Validators.required, Validators.maxLength(20), Validators.minLength(2)]],
      // tslint:disable-next-line: max-line-length
      email: [this.user.email, [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+[.]+[a-zA-Z0-9-.]*$')]],
      image: [this.user.image, [Validators.required]]
    });


    const avatar = document.getElementById('userimage');
    const image = document.getElementById('userBigImage');


    image.addEventListener('mouseover', () => { image.setAttribute('style', 'background-color: #dc4e6187 !important;'); });
    image.addEventListener('mouseout', () => { image.removeAttribute('style'); });

    this.userService.user$.subscribe(user => {
      this.identity = JSON.parse(user);
      this.user = this.identity;
      image.setAttribute('src', 'http://localhost:3000/api/users/get-avatar/' + this.user.image);
      avatar.setAttribute('src', 'http://localhost:3000/api/users/get-avatar/' + this.user.image);
    });
  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }

  updateUser() {
    this.user.email = this.editForm.get('email').value;
    this.user.name = this.editForm.get('name').value;
    this.user.surname = this.editForm.get('surname').value;
    this.userService.updateUser(this.user).subscribe((res: any) => {
      if (!res) {
        const err2 = new Error('El usuario no se ha actualizado');
        this.createAlert(err2);
      } else {
        this.user = res;
        localStorage.setItem('identity', JSON.stringify(this.user));
        this.identity = this.user;
        this.userService.user$.emit(JSON.stringify(this.user));

        if (this.filesToUpload) {
          this.userService.makeFileRequest([], this.filesToUpload, this.user._id)
          .then((result: any) => {
            this.user.image = result.image;
            localStorage.setItem('identity', JSON.stringify(this.user));
            this.identity = this.user;
            this.userService.user$.emit(JSON.stringify(this.user));
          });
        }
      }
    },
      err => {
        if (err) {
          this.createAlert(err);
        } else {
          const err3 = new Error('Usuario Actualizado Correctamente');
          this.createAlert(err3);
        }
      });

  }

  salirAtras() {
    this.router.navigate(['/principal/startmenu/you']);
  }

  fileChamgeEvent(fileInput: any) {
    this.filesToUpload = fileInput.target.files as Array<File>;
  }


}
