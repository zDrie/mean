import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { UserModel } from '../../../models/userModel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  user: UserModel;
  identity;
  imageRef = 'http://localhost:3000/api/users/get-avatar/';

  constructor(private userService: UserService, private router: Router) {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.user = this.identity;
   }

  ngOnInit() {
    const avatar = document.getElementById('userimage');
    const image = document.getElementById('userBigImage');

    this.userService.user$.subscribe(user => {
      console.log(JSON.parse(user));
      this.identity = JSON.parse(user);
      this.user = this.identity;
      image.setAttribute('src', 'http://localhost:3000/api/users/get-avatar/' + this.user.image);
      avatar.setAttribute('src', 'http://localhost:3000/api/users/get-avatar/' + this.user.image);

    });



  }



  editarUsuario() {
    this.router.navigate(['/principal/startmenu/you/edit']);
  }
}
