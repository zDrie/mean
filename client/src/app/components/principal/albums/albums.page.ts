import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AlbumService } from 'src/app/services/album.service';
import { AlertController } from '@ionic/angular';
import AlbumModel from 'src/app/models/albumModel';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.page.html',
  styleUrls: ['./albums.page.scss'],
})
export class AlbumsPage implements OnInit {

  identity;
  token;
  prevPage;
  nextPage;
  albums: AlbumModel[];
    // tslint:disable-next-line: max-line-length
    constructor(private userService: UserService, private alertCtrl: AlertController, private activatedRoute: ActivatedRoute, private albumService: AlbumService, private router: Router) { }

    ngOnInit() {
      this.identity = JSON.parse(this.userService.getIdentity());
      this.token = this.userService.getToken();

      if (!this.identity || !this.token) {
        this.router.navigate(['/login']);
      }
      this.getAlbums();
    }

    getAlbums() {
      this.activatedRoute.params.forEach((params: Params) => {
        let page = params.page;
        if (!page) {
          page = 1;
        } else {
          this.prevPage--;
          this.nextPage = this.prevPage + 2;
  
          if (this.prevPage === 0) {
            this.prevPage = 1;
            this.nextPage = 2;
          }
        }
        this.albumService.getAlbums(null, page).subscribe((res: any) => {
          if (!res) {
            this.router.navigate(['/']);
          } else {
            this.albums = res.albums;
          }
        }, (err) => {
          if (err) {
            this.createAlert(err);
          }
        });
      });

    }

    createAlert(err) {
      this.alertCtrl.create({
        header: 'Error: ' + err.status,
        message: err.error,
        buttons: [{
          text: 'Aceptar',
          role: 'cancel'
        }]
      }).then(alertEl => {
        alertEl.present();
      });
    }

    eliminarAlbum(id, title) {
      this.alertCtrl.create({
        header: 'Eliminar a ' + title + '?',
        message: 'estás apunto de eliminar a ' + title ,
        buttons: [{
          text: 'Agree next next',
          handler: () => {
            this.confirmado(id);
          }
        },
        {
          text: 'Noooo!',
          role: 'cancel'
        }]
      }).then(alertEl => {
        alertEl.present();
      });
    }

    confirmado(id) {
      this.albumService.deleteAlbum(id).subscribe((res: any) => {
        console.log(res);
        if (!res) {
          this.createAlert(new Error('No ha llegado respuesta del servidor'));

        } else {
          this.getAlbums();
        }
      }, (err) => {
        if (err) {
          this.createAlert(err);
        }
      });
    }


}
