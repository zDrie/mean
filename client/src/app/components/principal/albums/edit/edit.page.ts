import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { AlbumService } from 'src/app/services/album.service';
import AlbumModel from 'src/app/models/albumModel';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  identity;
  token;
  filesToUpload;
  imageRef = 'http://localhost:3000/api/artists/get-image/';
  album: AlbumModel = {
    _id: '',
    title: '',
    year: null,
    artist: this.activatedRoute.snapshot.params.id,
    description: '',
    image: ''
  };

  // tslint:disable-next-line: max-line-length
  constructor(private userService: UserService, private activatedRoute: ActivatedRoute, private alertCtrl: AlertController, private albumService: AlbumService, private router: Router, private formBuilder: FormBuilder) {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.token = this.userService.getToken();
    this.getAlbum();

    if (!this.identity || !this.token) {
      this.router.navigate(['/login']);
    }
    if (this.identity.rol !== 'ADMIN') {
      this.router.navigate(['/']);
    }

  }

  get title() {
    return this.editForm.get('title');
  }

  get description() {
    return this.editForm.get('description');
  }

  get year() {
    return this.editForm.get('year');
  }

  public errorMesssages = {
    title: [{ type: 'required', message: 'Title is required' },
    { type: 'minlength', message: 'Name cant be smaller than 2 characters' },
    { type: 'maxlength', message: 'Name cant be longer than 40 characters' }],
    year: [{ type: 'required', message: 'Year is required' },
    { type: 'pattern', message: 'Please enter a valid year YYYY' }],
    description: [{ type: 'required', message: 'Description is required' },
    { type: 'minlength', message: 'Surname cant be smaller than 10 characters' },
    { type: 'maxlength', message: 'Surname cant be longer than 512 characters' }]
  };

  editForm = this.formBuilder.group({
    title: [this.album.title, [Validators.required, Validators.maxLength(40), Validators.minLength(2)]],
    year: [this.album.year, [Validators.required, Validators.pattern('^[0-9-]{4}$')]],
    description: [this.album.description, [Validators.required, Validators.maxLength(512), Validators.minLength(10)]]
  });



  ngOnInit() {
    const image = document.getElementById('artistBigImage');

    image.addEventListener('mouseover', () => { image.setAttribute('style', 'background-color: #dc4e6187 !important;'); });
    image.addEventListener('mouseout', () => { image.removeAttribute('style'); });
  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }


  editAlbum() {
    this.album.title = this.editForm.get('title').value;
    this.album.description = this.editForm.get('description').value;
    this.albumService.updateAlbum(this.album).subscribe((res: any) => {
      if (!res) {
        const err2 = new Error('El usuario no se ha actualizado');
        this.createAlert(err2);
      } else {
        this.album = res;

        if (this.filesToUpload) {
          this.albumService.makeFileRequest([], this.filesToUpload, this.album._id)
            .then((result: any) => {
              this.album.image = result.image;
              this.getAlbum();
            });
          // // comentar segun convenga
          // this.router.navigate(['/principal/startmenu/artists/']);
        }
      }
    },
      (err: any) => {
        if (err) {
          this.createAlert(err);
        } else {
          const err3 = new Error('Artista Editado Correctamente');
          this.createAlert(err3);
        }
      }
    );

  }

  fileChangeEvent(fileInput: any) {
    this.filesToUpload = fileInput.target.files as Array<File>;
  }

  getAlbum() {
    const id = this.activatedRoute.snapshot.params.id;
    this.albumService.getAlbum(id).subscribe((res: any) => {
      if (!res) {
        this.createAlert(new Error('No hay respuesta del servidor'));
        this.router.navigate(['/']);
      } else {
        this.album = res;
      }
    }, (err: any) => {
      if (err) {
        this.createAlert(err);
        // this.router.navigate(['/']);
      } else {
        const err3 = new Error('IDK WHATS HAPPENING');
        this.createAlert(err3);
      }
    });
  }

  volver() {

    this.router.navigate(['/principal/startmenu/albums/1']);
  }
}
