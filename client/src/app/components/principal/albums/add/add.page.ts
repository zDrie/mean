import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import AlbumModel from 'src/app/models/albumModel';
import { UserService } from 'src/app/services/user.service';
import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ArtistService } from 'src/app/services/artist.service';
import { AlbumService } from 'src/app/services/album.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  identity;
  token;
  filesToUpload;
  prevPage;
  nextPage;



  // tslint:disable-next-line: max-line-length
  constructor(private userService: UserService, private activatedRoute: ActivatedRoute, private albumService: AlbumService, private alertCtrl: AlertController, private artistService: ArtistService, private router: Router, private formBuilder: FormBuilder) {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.token = this.userService.getToken();


    if (!this.identity || !this.token) {
      this.router.navigate(['/login']);
    }
    if (this.identity.rol !== 'ADMIN') {
      this.router.navigate(['/']);
    }

  }
  get title() {
    return this.addForm.get('title');
  }

  get description() {
    return this.addForm.get('description');
  }

  get year() {
    return this.addForm.get('year');
  }

  public errorMesssages = {
    title: [{ type: 'required', message: 'Title is required' },
    { type: 'minlength', message: 'Name cant be smaller than 2 characters' },
    { type: 'maxlength', message: 'Name cant be longer than 40 characters' }],
    year: [{ type: 'required', message: 'Year is required' },
    { type: 'pattern', message: 'Please enter a valid year YYYY' }],
    description: [{ type: 'required', message: 'Description is required' },
    { type: 'minlength', message: 'Surname cant be smaller than 10 characters' },
    { type: 'maxlength', message: 'Surname cant be longer than 512 characters' }]
  };

  album: AlbumModel = {
    _id: '',
    title: '',
    year: null,
    artist: this.activatedRoute.snapshot.params.id,
    description: '',
    image: ''
  };
  addForm = this.formBuilder.group({
    title: [this.album.title, [Validators.required, Validators.maxLength(40), Validators.minLength(2)]],
    year: [this.album.year, [Validators.required, Validators.pattern('^[0-9-]{4}$')]],
    description: [this.album.description, [Validators.required, Validators.maxLength(512), Validators.minLength(10)]]
  });


  ngOnInit() {

  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }


  addAlbum() {
    this.album.title = this.addForm.get('title').value;
    this.album.description = this.addForm.get('description').value;
    this.album.year = this.addForm.get('year').value;


    this.albumService.saveAlbum(this.album).subscribe((res: any) => {
      if (!res) {
        const err2 = new Error('El album no se ha guardado');
        this.createAlert(err2);
      } else {
        console.log(res);
        this.album = res;

        // descomentar cuando el edit album esté programado //
        this.router.navigate(['/principal/startmenu/albums-edit/' + this.album._id]);
      }
    },
     (err) => {
      if (err) {
        this.createAlert(err);
      } else {
        const err3 = new Error('Album Agregado Correctamente');
        this.createAlert(err3);
      }
    } );

  }

  salir() {
    this.router.navigate(['/']);
  }

}
