import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AlbumService } from 'src/app/services/album.service';
import { AlertController } from '@ionic/angular';
import { ArtistService } from 'src/app/services/artist.service';
import { Router, ActivatedRoute } from '@angular/router';
import AlbumModel from 'src/app/models/albumModel';
import { SongService } from 'src/app/services/song.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  identity;
  token;
  artist;
  album: AlbumModel = {
    _id: '',
    title: '',
    year: null,
    artist: '',
    description: '',
    image: ''
  };
  imageRef: 'http://localhost:3000/api/artists/get-image/';
  artistAlbums;
  canciones;



  // tslint:disable-next-line: max-line-length
  constructor(private userService: UserService, private songService: SongService, private albumService: AlbumService, private alertCtrl: AlertController, private artistService: ArtistService, protected router: Router, private activatedRoute: ActivatedRoute) {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.token = this.userService.getToken();

    if (!this.identity || !this.token) {
      this.router.navigate(['/login']);
    }

    this.getAlbum();

  }

  ngOnInit() {
  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }

  getAlbum() {
    const id = this.activatedRoute.snapshot.params.id;
    this.albumService.getAlbum(id).subscribe((res: any) => {
      if (!res) {
        this.createAlert(new Error('No hay respuesta del servidor'));
        this.router.navigate(['/']);
      } else {
        this.album = res;

        this.getArtist();
        this.getSongs();
      }
    }, (err: any) => {
      if (err) {
        this.createAlert(err);
        this.router.navigate(['/']);
      }
    });
  }

  getArtist() {
    this.artistService.getArtist((this.album.artist as any)._id).subscribe((ress: any) => {
      if (!ress) {
        this.createAlert(new Error('El servidor no ha enviado respuesta'));

      } else {
        this.artist = ress;
        this.getArtistAlbums();
      }
    }, (err) => {
      if (err) {
        this.createAlert(err);
        this.router.navigate(['/']);
      }
    });
  }

  addSongs() {
    this.router.navigate(['/principal/startmenu/songs-add/', this.album._id]);

  }

  editAlbum() {
    this.router.navigate(['/principal/startmenu/albums-edit/', this.album._id]);
  }


  getArtistAlbums() {
    this.albumService.getAlbums(this.artist._id).subscribe((res: any) => {
      if (!res) {
        this.createAlert(new Error('No ha llegado respuesta del servidor'));
      } else {
        this.artistAlbums = res.albums;
      }
    }, (err) => {
      if (err) {
        this.createAlert(err);
      }

    });
  }

  getSongs() {
    this.songService.getSongs(this.album._id).subscribe((res) => {
      this.canciones = res;
    }, (err) => {
      this.createAlert(err);
    });
  }

  eliminarSong(id, name) {
    this.alertCtrl.create({
      header: 'Eliminar a ' + name + '?',
      message: 'estás apunto de eliminar a ' + name ,
      buttons: [{
        text: 'Agree next next',
        handler: () => {
          this.confirmado(id);
        }
      },
      {
        text: 'Noooo!',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }

  confirmado(id) {
    this.songService.deleteSong(id).subscribe((res: any) => {
      if (!res) {
        this.createAlert(new Error('No ha llegado respuesta del servidor'));

      } else {
        this.getSongs();
      }
    }, (err) => {
      if (err) {
        this.createAlert(err);
      }
    });
  }

  reproducir(id) {
    this.songService.getSong(id).subscribe((res: any) => {
      if (res) {
        let song = res;
        this.artistService.getArtist(res.album.artist).subscribe((ress) => {
          song.album.artist = ress;
          this.songService.song$.emit(JSON.stringify(res));
        }, (err) => {
          if (err) {
            this.createAlert(err);
          }
        });
      } else {

      }
    }, (err) => {
      if (err) {
        this.createAlert(err);
      }
    });
  }

}
