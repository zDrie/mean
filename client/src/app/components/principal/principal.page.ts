import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import {UserService} from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})
export class PrincipalPage implements OnInit {
  identity;
  token;

  constructor(private menuCtrl: MenuController, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.identity = this.userService.getIdentity();
    this.token = this.userService.getToken();

    if (!this.identity || !this.token) {
      this.router.navigate(['/login']);
    }
  }


}
