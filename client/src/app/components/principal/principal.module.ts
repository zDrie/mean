import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PrincipalPage } from './principal.page';
import { StartmenuPageModule } from './startmenu/startmenu.module';

const routes: Routes = [
  {
    path: '',
    component: PrincipalPage
  },
  {
    path: 'startmenu',
    loadChildren: './startmenu/startmenu.module#StartmenuPageModule',
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    StartmenuPageModule
  ],
  declarations: [PrincipalPage],
  entryComponents: [PrincipalPage]

})
export class PrincipalPageModule {}
