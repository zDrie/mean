import { Component, OnInit } from '@angular/core';
import { ArtistModel } from 'src/app/models/artistModel';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ArtistService } from 'src/app/services/artist.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
  providers: [UserService, ArtistService]
})
export class AddPage implements OnInit {


  identity;
  token;
  filesToUpload;



  // tslint:disable-next-line: max-line-length
  constructor(private userService: UserService, private alertCtrl: AlertController, private artistService: ArtistService, private router: Router, private formBuilder: FormBuilder) {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.token = this.userService.getToken();


    if (!this.identity || !this.token) {
      this.router.navigate(['/login']);
    }
    if (this.identity.rol !== 'ADMIN') {
      this.router.navigate(['/']);
    }

  }
  get name() {
    return this.addForm.get('name');
  }

  get description() {
    return this.addForm.get('description');
  }

  public errorMesssages = {
    name: [{ type: 'required', message: 'Name is required' },
    { type: 'minlength', message: 'Name cant be smaller than 2 characters' },
    { type: 'maxlength', message: 'Name cant be longer than 40 characters' }],
    description: [{ type: 'required', message: 'Description is required' },
    { type: 'minlength', message: 'Surname cant be smaller than 10 characters' },
    { type: 'maxlength', message: 'Surname cant be longer than 512 characters' }]
  };
  artist: ArtistModel = {
    _id: '',
    name: '',
    description: '',
    image: ''
  };
  addForm = this.formBuilder.group({
    name: [this.artist.name, [Validators.required, Validators.maxLength(40), Validators.minLength(2)]],
    description: [this.artist.description, [Validators.required, Validators.maxLength(512), Validators.minLength(10)]]
  });


  ngOnInit() {

  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }


  addArtist() {
    this.artist.name = this.addForm.get('name').value;
    this.artist.description = this.addForm.get('description').value;
    this.artistService.saveArtist(this.artist).subscribe((res: any) => {
      if (!res) {
        const err2 = new Error('El usuario no se ha actualizado');
        this.createAlert(err2);
      } else {
        console.log(res);
        this.artist = res;
        this.artistService.artist$.emit(JSON.stringify(res.artist));


        // descomentar cuando el edit artis esté programado //
        this.router.navigate(['/principal/startmenu/artists-edit/' + this.artist._id]);
      }
    },
     (err) => {
      if (err) {
        this.createAlert(err);
      } else {
        const err3 = new Error('Artista Agregado Correctamente');
        this.createAlert(err3);
      }
    } );

  }

  salir() {
    this.artist = {
      _id: '',
      name: '',
      description: '',
      image: ''
    };
    this.router.navigate(['/']);
  }

}
