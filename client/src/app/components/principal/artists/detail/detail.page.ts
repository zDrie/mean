import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AlertController } from '@ionic/angular';
import { ArtistService } from 'src/app/services/artist.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ArtistModel } from 'src/app/models/artistModel';
import { AlbumService } from 'src/app/services/album.service';
import AlbumModel from 'src/app/models/albumModel';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  identity;
  albums: AlbumModel[] = [];
  token;
  artist: ArtistModel = {
    _id: '',
    name: '',
    description: '',
    image: ''
  };
  imageRef: 'http://localhost:3000/api/artists/get-image/';
nextPage = 2;
prevPage = 1;
page = 1;
tot = 0;

    // tslint:disable-next-line: max-line-length
    constructor(private userService: UserService, private albumService: AlbumService, private alertCtrl: AlertController, private artistService: ArtistService, private router: Router, private activatedRoute: ActivatedRoute) {
      this.identity = JSON.parse(this.userService.getIdentity());
      this.token = this.userService.getToken();


      if (!this.identity || !this.token) {
        this.router.navigate(['/login']);
      }

      this.getArtist();

    }

    ngOnInit() {
    }




    createAlert(err) {
      this.alertCtrl.create({
        header: 'Error: ' + err.status,
        message: err.error,
        buttons: [{
          text: 'Aceptar',
          role: 'cancel'
        }]
      }).then(alertEl => {
        alertEl.present();
      });
    }

    getArtist() {
      const id = this.activatedRoute.snapshot.params.id;
      this.artistService.getArtist(id).subscribe((res: any) => {
        if (!res) {
          this.createAlert(new Error('No hay respuesta del servidor'));
          this.router.navigate(['/']);
        } else {
          this.artist = res;
          document.getElementById('artist123').setAttribute('src', ('http://localhost:3000/api/artists/get-image/' + this.artist.image ));
          // obtener los albums del artista

          this.getAlbum();
        }
      }, (err: any) => {
        if (err) {
          this.createAlert(err);
          // this.router.navigate(['/']);
        } else {
          const err3 = new Error('IDK WHATS HAPPENING');
          this.createAlert(err3);
        }
      });
    }

    agregarAlbum() {
      this.router.navigate(['/principal/startmenu/albums-add/', this.artist._id]);
    }

    getAlbum() {
      this.albumService.getAlbums(this.artist._id, this.page).subscribe((ress: any) => {
        console.log(ress);
        if (!ress) {
          this.createAlert(new Error('El servidor no ha enviado respuesta'));

        } else {
          this.albums = ress.albums;
          this.tot = ress.pages;
        }
      }, (err) => {
        if (err) {
          this.createAlert(err);
          // this.router.navigate(['/']);
        } else {
          const err3 = new Error('IDK WHATS HAPPENING');
          this.createAlert(err3);
        }
      });
    }

    next() {
      if (this.nextPage === this.tot) {
        this.page = this.tot;
        this.nextPage = this.tot;
        this.prevPage = this.nextPage - 1;

      }
      console.log(this.prevPage);
      console.log(this.page);
      console.log(this.nextPage);
      this.getAlbum();
    }

    prev() {
      if (this.prevPage === 0) {
        this.prevPage = 1;
        this.page = 1;
        this.nextPage = 2;
      } else {
        this.page = this.prevPage;
        this.prevPage --;
        this.nextPage = this.prevPage + 2;
      }
      console.log(this.prevPage);
      console.log(this.page);
      console.log(this.nextPage);
      this.getAlbum();

    }


}
