import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AlertController } from '@ionic/angular';
import { ArtistService } from 'src/app/services/artist.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ArtistModel } from 'src/app/models/artistModel';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss']
})
export class EditPage implements OnInit {

  identity;
  token;
  filesToUpload;
  imageRef = 'http://localhost:3000/api/artists/get-image/';
  artist: ArtistModel = {
    _id: '',
    name: '',
    description: '',
    image: ''
  };

  // tslint:disable-next-line: max-line-length
  constructor(private userService: UserService, private activatedRoute: ActivatedRoute, private alertCtrl: AlertController, private artistService: ArtistService, private router: Router, private formBuilder: FormBuilder) {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.token = this.userService.getToken();
    this.getArtist();

    if (!this.identity || !this.token) {
      this.router.navigate(['/login']);
    }
    if (this.identity.rol !== 'ADMIN') {
      this.router.navigate(['/']);
    }

  }

  get name() {
    return this.editForm.get('name');
  }

  get description() {
    return this.editForm.get('description');
  }

  public errorMesssages = {
    name: [{ type: 'required', message: 'Name is required' },
    { type: 'minlength', message: 'Name cant be smaller than 2 characters' },
    { type: 'maxlength', message: 'Name cant be longer than 40 characters' }],
    description: [{ type: 'required', message: 'Description is required' },
    { type: 'minlength', message: 'Surname cant be smaller than 10 characters' },
    { type: 'maxlength', message: 'Surname cant be longer than 512 characters' }]
  };


  editForm = this.formBuilder.group({
    name: [this.artist.name, [Validators.required, Validators.maxLength(40), Validators.minLength(2)]],
    description: [this.artist.description, [Validators.required, Validators.maxLength(512), Validators.minLength(10)]]
  });

  ngOnInit() {
    const image = document.getElementById('artistBigImage');

    image.addEventListener('mouseover', () => { image.setAttribute('style', 'background-color: #dc4e6187 !important;'); });
    image.addEventListener('mouseout', () => { image.removeAttribute('style'); });
  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }


  editArtist() {
    this.artist.name = this.editForm.get('name').value;
    this.artist.description = this.editForm.get('description').value;
    this.artistService.updateArtist(this.artist).subscribe((res: any) => {
      if (!res) {
        const err2 = new Error('El usuario no se ha actualizado');
        this.createAlert(err2);
      } else {
        this.artist = res;
        this.artistService.artist$.emit(JSON.stringify(this.artist));
        console.log('emitido');

        if (this.filesToUpload) {
          this.artistService.makeFileRequest([], this.filesToUpload, this.artist._id)
            .then((result: any) => {
              this.artist.image = result.image;
              this.artistService.artist$.emit(JSON.stringify(this.artist));
              this.getArtist();
            });
          // // comentar segun convenga
          // this.router.navigate(['/principal/startmenu/artists/']);
        }
      }
    },
      (err: any) => {
        if (err) {
          this.createAlert(err);
        } else {
          const err3 = new Error('Artista Editado Correctamente');
          this.createAlert(err3);
        }
      }
    );

  }

  fileChangeEvent(fileInput: any) {
    this.filesToUpload = fileInput.target.files as Array<File>;
  }

  getArtist() {
    const id = this.activatedRoute.snapshot.params.id;
    this.artistService.getArtist(id).subscribe((res: any) => {
      if (!res) {
        this.createAlert(new Error('No hay respuesta del servidor'));
        this.router.navigate(['/']);
      } else {
        this.artist = res;
      }
    }, (err: any) => {
      if (err) {
        this.createAlert(err);
        // this.router.navigate(['/']);
      } else {
        const err3 = new Error('IDK WHATS HAPPENING');
        this.createAlert(err3);
      }
    });
  }

  volver() {

    this.router.navigate(['/principal/startmenu/artists']);
  }
}
