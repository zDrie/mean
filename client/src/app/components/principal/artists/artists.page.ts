import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ArtistModel } from '../../../models/artistModel';
import { ArtistService } from 'src/app/services/artist.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.page.html',
  styleUrls: ['./artists.page.scss']
})
export class ArtistsPage implements OnInit {
  identity;
  token;
  artists: ArtistModel[];
  imageRef: 'http://localhost:3000/api/artists/get-image/';
  url;
  nextPage;
  prevPage;
  // tslint:disable-next-line: max-line-length
  constructor(private userService: UserService, private alertCtrl: AlertController, private artistService: ArtistService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.nextPage = 1;
    this.prevPage = 1;
    this.getArtists();
  }

  ngOnInit() {
    this.identity = JSON.parse(this.userService.getIdentity());
    this.token = this.userService.getToken();


    if (!this.identity || !this.token) {
      this.router.navigate(['/login']);
    }



  }

  agregarArtista() {
    this.router.navigate(['/principal/startmenu/artists-add']);
  }

  getArtists() {
    this.activatedRoute.params.forEach((params: Params) => {
      let page = params.page;
      if (!page) {
        page = 1;
      } else {
        this.prevPage--;
        this.nextPage = this.prevPage + 2;

        if (this.prevPage === 0) {
          this.prevPage = 1;
          this.nextPage = 2;
        }
      }
      this.artistService.getArtists(page).subscribe((res: any) => {
        if (!res) {
          this.router.navigate(['/']);
        } else {
          this.artists = res.artists;
        }
      }, (err) => {
        if (err) {
          this.createAlert(err);
        } else {
          const err3 = new Error('Artista Agregado Correctamente');
          this.createAlert(err3);
        }
      });
    });
  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }

  eliminarArtist(id, name) {
    this.alertCtrl.create({
      header: 'Eliminar a ' + name + '?',
      message: 'estás apunto de eliminar a ' + name ,
      buttons: [{
        text: 'Agree next next',
        handler: () => {
          this.confirmado(id);
        }
      },
      {
        text: 'Noooo!',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }

  confirmado(id) {
    this.artistService.deleteArtist(id).subscribe((res: any) => {
      if (!res) {
        this.createAlert(new Error('No ha llegado respuesta del servidor'));

      } else {
        this.getArtists();
      }
    }, (err) => {
      if (err) {
        this.createAlert(err);
      } else {
        const err3 = new Error('Artista Agregado Correctamente');
        this.createAlert(err3);
      }
    });
  }

}
