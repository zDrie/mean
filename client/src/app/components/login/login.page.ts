import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';
import { UserModel } from '../../models/userModel';
import { FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from './mustMatch';
import { Howl, Howler } from 'howler';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  identity;
  token;


  // tslint:disable-next-line: max-line-length
  constructor(private alertCtrl: AlertController, private userService: UserService, private activatedRoute: ActivatedRoute, private router: Router, private formBuilder: FormBuilder) {
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  get name() {
    return this.registrationForm.get('name');
  }
  get surname() {
    return this.registrationForm.get('surname');
  }
  get email1() {
    return this.registrationForm.get('email1');
  }

  get email2() {
    return this.registrationForm.get('email2');
  }

  get password1() {
    return this.registrationForm.get('password1');
  }

  get password2() {
    return this.registrationForm.get('password2');
  }
  user: UserModel = {
    _id: '0',
    name: '',
    surname: '',
    password: '',
    email: '',
    rol: 'ROLE_USER',
    image: ''
  };

  registrationForm = this.formBuilder.group({
    name: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(2)]],
    surname: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(2)]],
    email1: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+[.]+[a-zA-Z0-9-.]*$')]],
    // tslint:disable-next-line: max-line-length
    email2: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+[.]+[a-zA-Z0-9-.]*$')]],
    password1: ['', [Validators.required, Validators.minLength(8)]],
    password2: ['', [Validators.required, Validators.minLength(8)]]
  }, { validator: [MustMatch('password1', 'password2'), MustMatch('email1', 'email2')] });

  loginForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+[.]+[a-zA-Z0-9-.]*$')]],
    password: ['', [Validators.required, Validators.maxLength(15)]]
  });

  public errorMesssages = {
    email: [{ type: 'required', message: 'Email is required' },
    { type: 'pattern', message: 'Please enter a valid email adress' }],
    password: [{ type: 'required', message: 'Password is required' },
    { type: 'maxlength', message: 'Password cant be longer than 15 characters' }],
    name: [{ type: 'required', message: 'Name is required' },
    { type: 'minlength', message: 'Name cant be smaller than 2 characters' },
    { type: 'maxlength', message: 'Name cant be longer than 20 characters' }],
    surname: [{ type: 'required', message: 'Surname is required' },
    { type: 'minlength', message: 'Surname cant be smaller than 2 characters' },
    { type: 'maxlength', message: 'Surname cant be longer than 20 characters' }],
    email1: [{ type: 'required', message: 'Email is required' },
    { type: 'pattern', message: 'Please enter a valid email adress' }],
    email2: [{ type: 'required', message: 'Email is required' },
    { type: 'mustMatch', message: 'Passwords must match' },
    { type: 'pattern', message: 'Please enter a valid email adress' }],
    password1: [{ type: 'required', message: 'Password is required' },
    { type: 'minlength', message: 'Surname cant be smaller than 8 characters' },
    { type: 'maxlength', message: 'Password cant be longer than 15 characters' }],
    password2: [{ type: 'required', message: 'Password is required' },
    { type: 'mustMatch', message: 'Passwords must match' },
    { type: 'minlength', message: 'Surname cant be smaller than 8 characters' }]
  };


  ngOnInit() {
    const sound = new Howl({ src: './assets/sound/1.mp3' });
    const avatar = document.getElementById('avatar');
    avatar.addEventListener('mouseover', () => { sound.play(); });
    avatar.addEventListener('mouseout', () => { sound.pause(); });
    this.identity = this.userService.getIdentity();
    this.token = this.userService.getToken();
  }

  login() {
    this.user.email = this.loginForm.get('email').value;
    this.user.password = this.loginForm.get('password').value;
    this.userService.login(this.user, false).subscribe((res2: any) => {
      this.identity = res2.user;
      if (this.identity._id) {
        localStorage.setItem('identity', JSON.stringify(this.identity));
        this.userService.login(this.user, true).subscribe((res: any) => {
          this.token = res.token;
          if (this.token.length >= 1) {
            localStorage.setItem('token', this.token);
            this.router.navigate(['/principal/startmenu/artists']);
          } else {
            const err = new Error('el token no es correcto');
            this.createAlert(err);
          }
        },
          err => {
            if (err) {
              this.createAlert(err);
            }
          });
      } else {
        const err = new Error('el usuario no ha podido logearse correctamente');
        this.createAlert(err);
      }
    },
      err => {
        this.createAlert(err);
      });
  }

  createAlert(err) {
    this.alertCtrl.create({
      header: 'Error: ' + err.status,
      message: err.error,
      buttons: [{
        text: 'Aceptar',
        role: 'cancel'
      }]
    }).then(alertEl => {
      alertEl.present();
    });
  }

  register() {
    delete this.user._id;
    this.user.email = this.registrationForm.get('email1').value;
    this.user.name = this.registrationForm.get('name').value;
    this.user.surname = this.registrationForm.get('surname').value;
    this.user.password = this.registrationForm.get('password1').value;
    // llama al servicio de usuario y luego redirecciona al login
    this.userService.saveUser(this.user).subscribe(res => {

      this.alertCtrl.create({
        header: 'Ya puedes iniciar sesión',
        message: this.user.name + ' ya hemos creado tu usuario :3',
        buttons: [{
          text: 'Aceptar',
          role: 'cancel'
        }]
      }).then(alertEl => {
        alertEl.present();
      });
    },
      err => this.createAlert(err));
  }
}
