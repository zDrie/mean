import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import AlbumModel from '../models/albumModel';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  constructor(private http: HttpClient) { }

  saveAlbum(album: AlbumModel) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });
    const params = JSON.stringify(album);

    return this.http.post('http://localhost:3000/api/albums', params, { headers });
  }

  getAlbums(id = null, page = 1) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });

    if (id == null) {
      return this.http.get('http://localhost:3000/api/albums/all/' + page, {headers} );

    } else {
      return this.http.get('http://localhost:3000/api/albums/all/'+ page + '/' + id, {headers} );
    }
  }

  getAlbum(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });

    return this.http.get('http://localhost:3000/api/albums/' + id, {headers} );
  }

  updateAlbum(album) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });
    const params = JSON.stringify(album);

    return this.http.put('http://localhost:3000/api/albums/update/' + album._id, params, { headers });
  }

  deleteAlbum(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });

    return this.http.delete('http://localhost:3000/api/albums/' + id, { headers });
  }

  makeFileRequest(params: Array<string>, files: Array<File>, id) {
    const token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();

      for (var i = 0 ; i < files.length ; i++) {
        formData.append('image', files[i], files[i].name);
      }
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      };
      xhr.open('POST', 'http://localhost:3000/api/albums/upload-image/' + id, true);
      xhr.setRequestHeader('authorization', token);
      xhr.send(formData);

    });
  }
}
