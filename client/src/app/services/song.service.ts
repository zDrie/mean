import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import SongModel from '../models/songModel';

@Injectable({
  providedIn: 'root'
})
export class SongService {
  
  song$ = new EventEmitter<string>();


  constructor(private http: HttpClient) { }

  saveSong(song: SongModel) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });
    const params = JSON.stringify(song);

    return this.http.post('http://localhost:3000/api/songs', params, { headers });
  }

  getSongs(id = null) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });

    if (id == null) {
      return this.http.get('http://localhost:3000/api/songs/all/', {headers} );

    } else {
      return this.http.get('http://localhost:3000/api/songs/all/' + id, {headers} );
    }
  }

  getSong(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });

    return this.http.get('http://localhost:3000/api/songs/' + id, {headers} );
  }

  updateSong(song) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });
    const params = JSON.stringify(song);

    return this.http.put('http://localhost:3000/api/songs/update/' + song._id, params, { headers });
  }

  deleteSong(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });

    return this.http.delete('http://localhost:3000/api/songs/' + id, { headers });
  }

  makeFileRequest(params: Array<string>, files: Array<File>, id) {
    const token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();

      for (var i = 0 ; i < files.length ; i++) {
        formData.append('file', files[i], files[i].name);
      }
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      };
      xhr.open('POST', 'http://localhost:3000/api/songs/upload-file/' + id, true);
      xhr.setRequestHeader('authorization', token);
      xhr.send(formData);

    });
  }

}
