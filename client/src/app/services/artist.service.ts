import { Injectable, EventEmitter } from '@angular/core';
import { ArtistModel } from '../models/artistModel';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams, HttpRequest } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  artist$ = new EventEmitter<string>(); 
  
  constructor(private http: HttpClient) { }

  saveArtist(artist: ArtistModel) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });
    const params = JSON.stringify(artist);

    return this.http.post('http://localhost:3000/api/artists', params, { headers });
  }

  getArtists(pagina) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });

    return this.http.get('http://localhost:3000/api/artists/all/' + pagina, {headers} );
  }

  getArtist(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });

    return this.http.get('http://localhost:3000/api/artists/' + id, {headers} );
  }

  updateArtist(artist) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });
    const params = JSON.stringify(artist);

    return this.http.put('http://localhost:3000/api/artists/update/' + artist._id, params, { headers });
  }

  deleteArtist(id) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });

    return this.http.delete('http://localhost:3000/api/artists/' + id, { headers });
  }

  makeFileRequest(params: Array<string>, files: Array<File>, id) {
    const token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();

      for (var i = 0 ; i < files.length ; i++) {
        formData.append('image', files[i], files[i].name);
      }
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      };
      xhr.open('POST', 'http://localhost:3000/api/artists/upload-image/' + id, true);
      xhr.setRequestHeader('authorization', token);
      xhr.send(formData);

    });
  }
}
