import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { UserModel } from '../models/userModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user$ = new EventEmitter<string>();

  constructor(private http: HttpClient) { }



  deleteUser(id: string) {
    // return this.http.delete('http://localhost:3000/api/games/' + id);
  }

  saveUser(user: UserModel) {
    return this.http.post('http://localhost:3000/api/users/register', user);
  }

  updateUser(updatedUser: UserModel) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      authorization: localStorage.getItem('token')
    });
    const params = JSON.stringify(updatedUser);

    return this.http.put('http://localhost:3000/api/users/update/' + updatedUser._id, params, {headers});

  }

  login(user, getHash) {
    if (getHash) {
      user.getHash = getHash;
    }
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post('http://localhost:3000/api/users/login', user, { headers });
  }

  getIdentity() {
    const identity = localStorage.getItem('identity');
    if (identity == '[object Object]' || identity == null) {
      return null;
    } else {
      return identity;
    }
  }

  makeFileRequest(params: Array<string>, files: Array<File>, id) {
    const token = this.getToken();
    console.log(token);
    return new Promise((resolve, reject) => {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();

      for (var i = 0 ; i < files.length ; i++) {
        formData.append('image', files[i], files[i].name);
      }
      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      };

      xhr.open('POST', 'http://localhost:3000/api/users/upload-image/' + id, true);
      xhr.setRequestHeader('authorization', token);
      xhr.send(formData);

    });

  }

  getToken() {
    if (!localStorage.getItem('token')) {
      return null;
    } else {
      return localStorage.getItem('token');
    }
  }

  logOut() {
    localStorage.clear();
  }





}
